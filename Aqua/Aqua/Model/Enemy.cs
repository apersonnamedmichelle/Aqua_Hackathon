﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Factory;
using Aqua.Movement;
using Aqua.Update;
using Microsoft.Xna.Framework;

namespace Aqua.Model
{
    public class Enemy : BaseObject
    {
        private readonly IFactory<Bullet> _bulletFactory;
        private readonly List<BaseObject> _bulletSource;

        private Vector2 PlayerPosition => _bulletSource?.FirstOrDefault(x => x is Player)?.Position ??
                                          new Vector2(_random.Next(10, 700), 400);

        private int _lastShot = 100;
        private int _counter;

        private static Random _random = new Random();

        public Enemy(IMovementController controller, IFactory<Bullet> bulletFactory, List<BaseObject> bulletSource) : base(controller)
        {
            if (bulletFactory == null)
            {
                throw new ArgumentException(nameof(bulletFactory));
            }
            if (bulletSource == null)
            {
                throw new ArgumentException(nameof(bulletSource));
            }

            _bulletFactory = bulletFactory;
            _bulletSource = bulletSource;
        }

        public override void Update(GameTime gameTime)
        {
            _counter++;
            _movementController.Move(this);
            if (_counter >= _lastShot)
            {
                _counter = 0;

                Shoot(PlayerPosition);
            }
        }

        public virtual void Shoot(Vector2 target)
        {
            const int bulletSpeed = 1;

            var velocity = GetDirection(Position, target);

            var bullet = _bulletFactory.Create(new object[]
            {
                Position.X,
                Position.Y,
                velocity.X * bulletSpeed,
                velocity.Y * bulletSpeed
            });

            OnShoot(bullet);
        }

        private Vector2 GetDirection(Vector2 source, Vector2 dest)
        {
            var dX = dest.X - source.X;
            var dY = dest.Y - source.Y;

            var mag = Math.Sqrt(dX*dX + dY*dY);

            var x = Convert.ToSingle(dX/mag);
            var y = Convert.ToSingle(dY/mag);

            return new Vector2(x, y);
        }

        private void OnShoot(Bullet bullet)
        {
            _bulletSource.Add(bullet);
        }

        public override void CollisionDetection(BaseObject other, List<BaseObject> objectlist, Game1 fuck)
        {
            if (CollidesWith(other, true) && other is Bullet)
            {
                Bullet shitty = (Bullet)other;
                if (shitty.playerBullet)
                {
                    kill(other);
                    objectlist.Remove(this);
                }
            }
        }
        public void kill(BaseObject other)
        {
            _bulletSource.Remove(other);
        }
    }
}
