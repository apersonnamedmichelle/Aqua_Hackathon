﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Movement;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Aqua.Model
{
    public class Bullet : BaseObject
    {
        public bool playerBullet;
        public Bullet(IMovementController controller) : base(controller)
        {
            playerBullet = false;
        }
        public bool get()
        {
            return playerBullet;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (this.playerBullet == false)
            {
                spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, (int)this.Position.Y, 64, 64), Color.White);
            }
            else
            {
                spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, ((int)this.Position.Y - 100), 64, 200), Color.White);
            }
        }
    }
}
