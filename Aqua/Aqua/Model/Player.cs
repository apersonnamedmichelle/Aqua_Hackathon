﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Movement;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Aqua.Factory;
using Microsoft.Xna.Framework;
using Aqua.Input;
using Aqua.Enum;

namespace Aqua.Model
{
    public class Player : BaseObject
    {
        private readonly IFactory<Bullet> _bulletFactory;
        private readonly List<BaseObject> _bulletSource;
        private bool _drawFlipped = true;
        Inputcontroller userinput;
        int motherfuckingcounter;
        public new int Height=> 64;
        public new int Width => 64;
        public Player(IMovementController controller, IFactory<Bullet> bulletFactory, List<BaseObject> bulletSource) : base(controller)
        {
            _bulletFactory = bulletFactory;
            _bulletSource = bulletSource;
            motherfuckingcounter = 0;
            userinput = Inputcontroller.Instance;
        }

        
        
        public override void Update(GameTime gameTime)
        {

            string textureName = _spriteController.HandleSpriteAnimation(this, gameTime);
            if (!string.Equals(textureName, ""))
            {
                this.Texture = ContentManager.Load<Texture2D>(textureName);
            }

            _movementController.Move(this);
            motherfuckingcounter++;

            if (motherfuckingcounter >= 30)
            {
                if (userinput.space)
                {
                    this.Shoot();
                }
                motherfuckingcounter = 0;
            }
            base.Update(gameTime);

        }

        public override bool CollidesWith(BaseObject other, bool calcPerPixel)
        {
            // Get dimensions of texture
            int widthOther = other.Texture.Width;
            int heightOther = other.Texture.Height;
            int widthMe = 64;
            int heightMe = 64;

            if (calcPerPixel &&
                ((Math.Min(widthOther, heightOther) > 100) ||
                (Math.Min(widthMe, heightMe) > 100)))
            {
                return Bounds.Intersects(other.Bounds)
                    && PerPixelCollision(this, other);
            }

            return Bounds.Intersects(other.Bounds);
        }

        public override void CollisionDetection(BaseObject other, List<BaseObject> objectlist, Game1 _gameMode)
        {
            if(CollidesWith(other, true) == true)
            {
                if(other is Bullet)
                {
                    Bullet shit = (Bullet)other;
                    if (shit.playerBullet == false)
                    {
                        _bulletSource.Remove(other);
                        _gameMode.GameMode = GameMode.MiniGame;
                        this.Health--;
                    }

                }
                else if(other is Enemy)
                {
                    objectlist.Remove(other);
                    _gameMode.GameMode = GameMode.MiniGame;
                    this.Health--;
                }
                
            }
        }

        public override bool PerPixelCollision(BaseObject a, BaseObject b)
        {
            Color[] bitsA = new Color[(a.Texture.Width) * (a.Texture.Height)];
            a.Texture.GetData(bitsA);
            Color[] bitsB = new Color[b.Texture.Width * b.Texture.Height];
            b.Texture.GetData(bitsB);

            int x1 = Math.Max(a.Bounds.X, b.Bounds.X);
            int x2 = Math.Min(a.Bounds.X + a.Bounds.Width, b.Bounds.X + b.Bounds.Width);

            int y1 = Math.Max(a.Bounds.Y, b.Bounds.Y);
            int y2 = Math.Min(a.Bounds.Y + a.Bounds.Height, b.Bounds.Y + b.Bounds.Height);

            for (int y = y1; y < y2; ++y)
            {
                for (int x = x1; x < x2; ++x)
                {
                    Color a1 = bitsA[(x - a.Bounds.X) + (y - a.Bounds.Y) * a.Texture.Width];
                    Color b1 = bitsB[(x - b.Bounds.X) + (y - b.Bounds.Y) * b.Texture.Width];

                    if (a1.A != 0 && b1.A != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }


    public virtual void Shoot()
        {
            const int bulletSpeed = 1;

            var velocity = new Vector2(0, -10);

            var bullet = _bulletFactory.Create(new object[]
            {
                Position.X,
                Position.Y,
                velocity.X * bulletSpeed,
                velocity.Y * bulletSpeed
            });
            bullet.playerBullet = true;
            bullet.Texture = BulletFactory._bulletTexture2MegaLaser;
            OnShoot(bullet);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            if (Direction == Enum.MovementState.Right)
            {
                _drawFlipped = false;
                spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, (int)this.Position.Y, 64, 64), Color.White);
            }
            else if (Direction == Enum.MovementState.Left)
            {
                _drawFlipped = true;
                spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, (int)this.Position.Y, 64, 64), null, Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 0f);
            }
            else if (Direction == Enum.MovementState.Up)
            {
                spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, (int)this.Position.Y, 64, 64), null, Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 0f);
            }
            else if (Direction == Enum.MovementState.Idle)
            {
                if (_drawFlipped == true)
                {
                    spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, (int)this.Position.Y, 64, 64), null, Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 0f);
                }
                else
                {
                    spriteBatch.Draw(Texture, new Rectangle((int)this.Position.X, (int)this.Position.Y, 64, 64), Color.White);
                }
            }
            
            
        }


        private void OnShoot(Bullet bullet)
        {
            _bulletSource.Add(bullet);
        }

    }
}
