﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Aqua.Enum;
using Microsoft.Xna.Framework.Content;

namespace Aqua.Model
{
    public abstract class BaseObject : IMoveable
    {
        protected SpriteController _spriteController;
        protected readonly IMovementController _movementController;

        public Texture2D Texture { get; set; }

        private Rectangle bounds;


        #region IMoveable

        /// <summary>
        /// The position of the object on screen
        /// </summary>
        public Vector2 Position { get; set; }

        /// <summary>
        /// The direction the object is moving.
        /// </summary>
        public MovementState Direction { get; set; }
        #endregion

        public bool Active { get; set; }

        public int Health { get; set; }

        public int Width => Texture.Width;

        public int Height => Texture.Height;

        public static ContentManager ContentManager { get; set; }

        public Vector2 Velocity { get; set; }

        public BaseObject(IMovementController controller)
        {
            //if (controller == null)
            //{
            //    throw new ArgumentException(nameof(controller));
            //}

            _movementController = controller;
            _spriteController = new SpriteController(0);
        }


        public void Initialize(Texture2D texture, Vector2 position)
        {
            if (texture == null)
            {
                throw new ArgumentException(nameof(texture));
            }

            if (position == null)
            {
                throw new ArgumentException(nameof(position));
            }
            Texture = texture;
            Position = position;
            bounds = Rectangle.Empty;
        }
        
        public virtual void Update(GameTime gameTime)
        {
            _movementController.Move(this);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position);
        }

        public virtual void CollisionDetection(BaseObject other, List<BaseObject> objectlist, Game1 _gameMode)
        {
            // Default behavior uses per-pixel collision detection
            CollidesWith(other, true);
        }
        public virtual Rectangle Bounds
        {
            get
            {
                return new Rectangle(
                    (int)Position.X - Texture.Width,
                    (int)Position.Y - Texture.Height,
                    Texture.Width,
                    Texture.Height);
            }

        }

        public virtual bool CollidesWith(BaseObject other, bool calcPerPixel)
        {
            // Get dimensions of texture
            int widthOther = other.Texture.Width;
            int heightOther = other.Texture.Height;
            int widthMe = Texture.Width;
            int heightMe = Texture.Height;

            if (calcPerPixel &&
                ((Math.Min(widthOther, heightOther) > 100) ||
                (Math.Min(widthMe, heightMe) > 100)))
            {
                return Bounds.Intersects(other.Bounds)
                    && PerPixelCollision(this, other);
            }

            return Bounds.Intersects(other.Bounds);
        }

         virtual public bool PerPixelCollision(BaseObject a, BaseObject b)
        {
            Color[] bitsA = new Color[a.Texture.Width * a.Texture.Height];
            a.Texture.GetData(bitsA);
            Color[] bitsB = new Color[b.Texture.Width * b.Texture.Height];
            b.Texture.GetData(bitsB);

            int x1 = Math.Max(a.Bounds.X, b.Bounds.X);
            int x2 = Math.Min(a.Bounds.X + a.Bounds.Width, b.Bounds.X + b.Bounds.Width);

            int y1 = Math.Max(a.Bounds.Y, b.Bounds.Y);
            int y2 = Math.Min(a.Bounds.Y + a.Bounds.Height, b.Bounds.Y + b.Bounds.Height);

            for (int y = y1; y < y2; ++y)
            {
                for (int x = x1; x < x2; ++x)
                {
                    Color a1 = bitsA[(x - a.Bounds.X) + (y - a.Bounds.Y) * a.Texture.Width];
                    Color b1 = bitsB[(x - b.Bounds.X) + (y - b.Bounds.Y) * b.Texture.Width];

                    if (a1.A != 0 && b1.A != 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
       
    }
}
