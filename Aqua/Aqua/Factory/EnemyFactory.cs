﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Model;
using Aqua.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua.Factory
{
    class EnemyFactory : IFactory<Enemy>
    {
        private readonly Dictionary<string, IMovementController> _movementByName;
        private readonly Dictionary<string, Texture2D> _texture2Ds;
        private readonly IFactory<Bullet> _bulletFactory;
        private readonly List<BaseObject> _bullets;

        public EnemyFactory(
            Dictionary<string, IMovementController> movementControllers, 
            Dictionary<string, Texture2D> texturesByName,
            IFactory<Bullet> bulletFactory,
            List<BaseObject> bullets)
        {
            if (movementControllers == null)
            {
                throw new ArgumentException(nameof(movementControllers));
            }
            if (bulletFactory == null)
            {
                throw new ArgumentException(nameof(bulletFactory));
            }
            if (bullets == null)
            {
                throw new ArgumentException(nameof(bullets));
            }

            _texture2Ds = texturesByName;
            _movementByName = movementControllers;
            _bulletFactory = bulletFactory;
            _bullets = bullets;
        }

        public Enemy Create(object[] @params)
        {
            if (@params.Length < 6) { return null; }

            var x = @params[0] as float? ?? -1;
            var y = @params[1] as float? ?? -1;
            var velX = @params[2] as float? ?? -1;
            var velY = @params[3] as float? ?? -1;
            var movement = @params[4] as string ?? string.Empty;
            var type = @params[5] as string ?? string.Empty;

            return Create(x, y, velX, velY, movement, type);
        }

        private Enemy Create(float x, float y, float velX, float velY, string movement, string type)
        {
            var enemy = new Enemy(_movementByName["random"], _bulletFactory, _bullets)
            {
                Active = true,
                Position = new Vector2(x, y),
                Velocity = new Vector2(velX, velY),
                Health = 1,
                Texture = _texture2Ds[type]
            };

            return enemy;
        }
    }
}
