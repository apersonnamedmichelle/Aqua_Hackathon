﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Model;
using Aqua.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Aqua.Factory
{
    class PlayerFactory : IFactory<Player>
    {
        // umm 
        IMovementController _playerMovementController = new PlayerMovementController();
        GraphicsDevice graphicsDevice;
        ContentManager contentManager;
        private readonly IFactory<Bullet> _bulletFactory;
        private readonly List<BaseObject> _bullets;
        public PlayerFactory(GraphicsDevice gd, ContentManager newContentManager, IFactory<Bullet> bulletFactory,
            List<BaseObject> bullets)
        {
            graphicsDevice = gd;
            contentManager = newContentManager;
            _bulletFactory = bulletFactory;
            _bullets = bullets;
        }

        public Player Create(object[] @params)
        {
            if (@params.Length < 3) { return null; }

            var x = @params[0] as int? ?? -1;
            var y = @params[1] as int? ?? -1;
            var life = @params[2] as int? ?? -1;

            return new Player(_playerMovementController, _bulletFactory, _bullets)
            {
                Position = new Vector2(x, y),
                Texture = contentManager.Load<Texture2D>("kirby"),
                Health = life
            };
            
        }
    }
}
