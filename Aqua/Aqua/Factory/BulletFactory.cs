﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Model;
using Aqua.Movement;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua.Factory
{
    public class BulletFactory : IFactory<Bullet>
    {
        private readonly Dictionary<string, IMovementController> _movementByName;

        private readonly Texture2D _bulletTexture2D;
        public static Texture2D _bulletTexture2MegaLaser;


        public BulletFactory(Dictionary<string, IMovementController> movementControllers, ContentManager newContentManager)
        {
            if (movementControllers == null)
            {
                throw new ArgumentException(nameof(movementControllers));
            }

            _movementByName = movementControllers;
            _bulletTexture2D = newContentManager.Load<Texture2D>("EnemyProjectile1");
            _bulletTexture2MegaLaser = newContentManager.Load<Texture2D>("MegaLaser");
        }

        public Bullet Create(object[] @params)
        {
            if (@params.Length < 4) { return null; }

            var x = @params[0] as float? ?? -1;
            var y = @params[1] as float? ?? -1;
            var velX = @params[2] as float? ?? -1;
            var velY = @params[3] as float? ?? -1;

            return Create(x, y, velX, velY);
        }

        private Bullet Create(float x, float y, float velX, float velY)
        {
            var bullet = new Bullet(_movementByName["straight"])
            {
                Active = true,
                Position = new Vector2(x,y),
                Velocity = new Vector2(velX, velY),
                Texture = _bulletTexture2D,
                Health = 1
            };

            return bullet;
        }
    }
}
