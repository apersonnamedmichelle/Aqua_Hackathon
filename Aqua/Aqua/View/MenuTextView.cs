﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.ViewModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua.View
{
    public class MenuTextView
    {
        private readonly MenuViewModel _viewModel;
        private readonly SpriteFont _font;

        public MenuTextView(MenuViewModel viewModel, SpriteFont font)
        {
            _viewModel = viewModel;
            _font = font;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            var start = 50;

            if (_viewModel.GameOver)
            {
                spriteBatch.DrawString(_font, "GAME OVER", new Vector2(350, start), Color.Red);

                start += 75;
            }

            foreach (var o in _viewModel.Options)
            {
                if (_viewModel.SelectedIndex < 0 || _viewModel.SelectedIndex >= _viewModel.Options.Length)
                {
                    return;
                }

                var color = _viewModel.Options[_viewModel.SelectedIndex] == o
                    ? Color.Red
                    : Color.White;

                spriteBatch.DrawString(_font, o, new Vector2(25, start), color);
                start += 40;
            }
        }
    }
}
