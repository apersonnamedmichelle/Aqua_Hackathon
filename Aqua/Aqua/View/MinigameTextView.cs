﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.ViewModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua.View
{
    class MinigameTextView
    {
        private readonly MinigameViewModel _viewModel;
        private readonly SpriteFont _font;

        public bool IsDone { get; set; }

        public bool Fail { get; set; }

        public string Error { get; set; }

        public string Instruction { get; set; }

        public string Answer { get; set; }

        public string UserInput { get; set; }

        public int TimeLimit { get; set; }

        public int Time { get; set; }

        public MinigameTextView(MinigameViewModel viewModel, SpriteFont font)
        {
            if (viewModel == null)
            {
                throw new ArgumentException(nameof(viewModel));
            }

            _viewModel = viewModel;
            Time = 0;
            _font = font;
        }

        public virtual void Update()
        {
            UserInput = _viewModel.UserInput;
            Time++;

            if (Time >= TimeLimit)
            {
                Time = 0;
                Finish();
            }
        }

        public void Clear()
        {
            Time = 0;
            Instruction = string.Empty;
            UserInput = string.Empty;
            Fail = false;
            IsDone = false;
        }

        public void Finish()
        {
            IsDone = true;
            Time = 0;
            Fail = string.Compare(UserInput, Answer, StringComparison.CurrentCultureIgnoreCase) != 0;
            _viewModel.WaitingForUserToExit = Fail;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Fail)
            {
                // failure case
                spriteBatch.DrawString(_font, Error, new Vector2(25, 25), Color.White);
                Time = 0;
                spriteBatch.DrawString(_font, "Press 'enter' to return to game.", new Vector2(25, 75), Color.White);
            }
            else
            {
                // game running
                spriteBatch.DrawString(_font, Instruction, new Vector2(25, 25), Color.White);
                
                spriteBatch.DrawString(_font, UserInput, new Vector2(25, 75), Color.White);
                
                var timeLeft = Convert.ToInt32(((float)(TimeLimit - Time) / (float)TimeLimit) * 100);

                spriteBatch.DrawString(_font, $"Time left : {timeLeft}%", new Vector2(25, 100), Color.White);

            }
        }
    }
}
