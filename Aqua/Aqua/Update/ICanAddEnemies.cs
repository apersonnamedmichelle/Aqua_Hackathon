﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Update
{
    public interface ICanAddEnemies
    {
        void AddEnemy(string type, object[] @params);
    }
}
