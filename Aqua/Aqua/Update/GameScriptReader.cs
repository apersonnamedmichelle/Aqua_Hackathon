﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Aqua.Update
{
    public class GameScriptReader
    {
        private readonly ICanAddEnemies _enemyController;

        private XElement _scriptRoot;

        public GameScriptReader(ICanAddEnemies enemyController, XElement source)
        {
            if (enemyController == null)
            {
                throw new ArgumentException(nameof(enemyController));
            }
            if (source == null)
            {
                throw new ArgumentException(nameof(source));
            }

            _enemyController = enemyController;
            _scriptRoot = source;
        }

        public void ReadTime(string time)
        {
            var elements = _scriptRoot.Elements("Event").Where(x => x.Attribute("time")?.Value == time);

            foreach (var e in elements)
            {
                Handle(e);
            }
        }

        private void Handle(XElement @event)
        {
            var type = @event.Element("Type")?.Value ?? string.Empty;

            switch (type)
            {
                case "Enemy":

                    var enemyType = @event.Element("Details")?.Element("EnemyType")?.Value ?? string.Empty;

                    _enemyController.AddEnemy(enemyType, new object[0]); // todo: fix

                    break;
            }
        }
    }
}
