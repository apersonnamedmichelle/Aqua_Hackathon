﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Model;
using Microsoft.Xna.Framework;

namespace Aqua.Update
{
    interface ICanAddBullets
    {
        void AddBullet(Bullet bullet);
    }
}
