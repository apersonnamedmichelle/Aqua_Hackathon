﻿using Aqua.Enum;
using Aqua.Game;
using Aqua.Update;
using Aqua.Movement;
using Aqua.ViewModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Aqua.Input;

namespace Aqua
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game, IPlayable, IHasGameMode
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public GameScriptReader Reader { get; set; }
        public GameViewModel MainGameViewModel { get; set; }
        public MinigameViewModel MinigameViewModel { get; set; }
        public MenuViewModel MenuViewModel { get; set; }
        public IMovementController movement = new PlayerMovementController();
        public GameMode GameMode { get; set; }
        public GameMode PrevMode { get; set; }
        Inputcontroller userinput;

        public bool GameOver { get; set; }


        #region IPlayable

        public string About => GameMode == GameMode.MainGame
            ? "This is the main game"
            : GameMode == GameMode.MiniGame
                ? "This is a minigame"
                : "This is the menu";

        public void Play()
        {
            GameMode = PrevMode;

            if (GameOver)
            {
                MainGameViewModel.Restart();
                GameOver = false;
            }
        }

        public void Quit()
        {
            this.Exit();
        }

        #endregion

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            userinput = Inputcontroller.Instance;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();

            var composer = new GameComposer();
            composer.Compose(this);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Tiles.Content = Content;
            SpriteController.ContentManager = Content;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            userinput.updateinput();

            UpdateModel(gameTime);
        }

        private void UpdateModel(GameTime gameTime)
        {
            switch (GameMode)
            {
                case GameMode.Menu:
                    MenuViewModel.HandleUserInput();

                    break;

                case GameMode.MainGame:

                    
                    MainGameViewModel.Update(gameTime);
                    //Reader.ReadTime(MainGameViewModel.GameCounter.ToString());

                    break;

                case GameMode.MiniGame:

                    MinigameViewModel.Update();

                    break;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            switch (GameMode)
            {
                case GameMode.MainGame:

                    if (userinput.escape)
                    {
                        GameMode = GameMode.Menu;
                        break;
                    }

                    GraphicsDevice.Clear(Color.CornflowerBlue);
                    MainGameViewModel._map.Draw(spriteBatch);
                    MainGameViewModel.Draw(spriteBatch);

                    break;
                
                case GameMode.Menu:
                    GraphicsDevice.Clear(Color.Purple);
                    MenuViewModel.DrawBackground(spriteBatch);
                    // todo: Draw menu image
                    MenuViewModel.Draw(spriteBatch);
                    break;

                case GameMode.MiniGame:

                    GraphicsDevice.Clear(Color.Black);
                    MinigameViewModel.Draw(spriteBatch);

                    break;
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
