﻿using System;
using System.Collections.Generic;
using System.Linq;
using Aqua.Game;
using Aqua.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Runtime.Remoting.Contexts;
using Microsoft.Xna.Framework;
using Aqua.View;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua.ViewModel
{
    public class MenuViewModel
    {
        private readonly IPlayable _game;
        private readonly Inputcontroller _inputController;
        private Rectangle Window;
        public Texture2D Background { get; set; }
        
        private readonly MenuTextView _view;

        public bool GameOver => _game.GameOver;

        private Dictionary<string, bool> _prevState;

        public int SelectedIndex = 0;
        
        public MenuViewModel(IPlayable game, Inputcontroller input, SpriteFont font, GraphicsDevice newGraphicsDevice, ContentManager newContent)
        {
            if (game == null)
            {
                throw new ArgumentException(nameof(game));
            }
            //For drawing the background
            int height = newGraphicsDevice.PresentationParameters.Bounds.Height;
            int width = newGraphicsDevice.PresentationParameters.Bounds.Width;
            Window = new Rectangle(0, 0, width, height);
            Background = newContent.Load<Texture2D>("8-bit-purple-sunset-40894-1920x1080");
            // init state
            _prevState = GetState();

            _game = game;
            _inputController = input;
            
            _view = new MenuTextView(this, font);
        }

        public void HandleUserInput()
        {
            var presses = GetState().Where(x => x.Value != _prevState[x.Key] && x.Value).Select(x => x.Key).ToList();

            foreach (var p in presses)
            {
                switch (p)
                {
                    case "enter":
                        Handle(Options[SelectedIndex]);
                        break;
                    case "back": // doesn't do anything for this class
                        break;
                    case "up":
                        SelectedIndex = (SelectedIndex - 1) % Options.Length;

                        if (SelectedIndex < 0) SelectedIndex = Options.Length - 1;
                        break;
                    case "down":
                        SelectedIndex = (SelectedIndex + 1) % Options.Length;
                        break;
                }
            }

            _prevState = GetState();
        }

        private Dictionary<string, bool> GetState()
        {
            return new Dictionary<string, bool>
            {
                { "enter", _inputController?.enter ?? false },
                { "back", _inputController?.backspace ?? false },
                { "up", _inputController?.up ?? false},
                { "down", _inputController?.down ?? false },
                { "left", _inputController?.left ?? false },
                { "right", _inputController?.right ?? false },
            };
        }

        public string[] Options { get; } = new[]
        {
            "Play",
            //"About",
            "Quit"
        };

        private void Handle(string selection)
        {
            switch (selection)
            {
                case "Play":

                    _game.Play();

                    break;

                case "About":

                    //Display(_game.About);

                    break;

                case "Quit":

                    _game.Quit();

                    break;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            _view.Draw(spriteBatch);
        }
        public void DrawBackground(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Background,Window, Color.CornflowerBlue );
        }
    }
}
