﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Enum;
using Aqua.Factory;
using Aqua.Game;
using Aqua.Model;
using Aqua.Update;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Aqua.ViewModel
{
    public class GameViewModel : ICanAddEnemies, ICanAddBullets, IHasMinigame
    {
        private readonly IFactory<Enemy> _enemyFactory;
        private readonly IFactory<Player> _playerFactory;
        private readonly Game1 _gameMode;
        public TileMap _map;

        private SpriteFont _font;
        private Player _player;

        public List<BaseObject> GameObjects { get; }

        public int GameCounter { get; private set; }

        public GameViewModel(
            IFactory<Player> playerFactory,
            IFactory<Enemy> enemyFactory,
            List<BaseObject> gameObjects,
            Game1 gameMode,
            SpriteFont font)
        {
            if (playerFactory == null)
            {
                throw new ArgumentException(nameof(playerFactory));
            }
            if (enemyFactory == null)
            {
                throw new ArgumentException(nameof(enemyFactory));
            }
            if (gameObjects == null)
            {
                throw new ArgumentException(nameof(gameObjects));
            }

            GameObjects = gameObjects;
            _font = font;
            _gameMode = gameMode;

            _map = new TileMap();

            _map.Generate(new int[,] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            }, 64, 50);

            _map.Generate(new int[,] {
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0 },
            }, 150, 50);
            
            _player = playerFactory.Create(new object[]{ 500, 500, 3 });
            GameObjects.Add(_player);
            
            _enemyFactory = enemyFactory;
            _playerFactory = playerFactory;

            Restart();
        }

        public void Restart()
        {
            GameObjects.Clear();
            GameCounter = 0;
            _player = _playerFactory.Create(new object[] { 0, 0, 3 });
            _player.Health = 5;
            GameObjects.Add(_player);
        }

        public void Update(GameTime gameTime)
        {
            GameCounter++;
            GameObjects.ToList().ForEach(x => _player.CollisionDetection(x, GameObjects, _gameMode));
            try {
                for (int i = 0; i < GameObjects.Count(); i++)
                {
                    if (GameObjects[i] is Enemy)
                    {
                        for (int j = 0; j < GameObjects.Count(); j++)
                        {
                            if (i != j)
                            {
                                GameObjects[i].CollisionDetection(GameObjects[j], GameObjects, _gameMode);
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                //NOTHING WOOT WOOT
            }

            _player.Update(gameTime);
            
            GameObjects.ToList().ForEach(x => x.Update(gameTime));

            if (GameCounter > 5)
            {
                if (new Random(Guid.NewGuid().GetHashCode()).Next(0, 1000) > 980)
                {
                    RandomlyGenerateEnemy();
                }
            }

            if (_player.Health < 1)
            { 
                _gameMode.GameMode = GameMode.Menu;
                _gameMode.GameOver = true;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            GameObjects.ForEach(x => x.Draw(spriteBatch));
            // 
            String scoreText = String.Concat("Score: ", GameCounter.ToString());
            String healthText = String.Concat("Health: ", _player.Health.ToString());
            spriteBatch.DrawString(_font, scoreText, new Vector2(5,5), Color.Bisque);
            spriteBatch.DrawString(_font, healthText, new Vector2(5,25), Color.Bisque);
            _map.Draw(spriteBatch);
        }

        public void RandomlyGenerateEnemy()
        {
            var rand = new Random(Guid.NewGuid().GetHashCode());

            AddEnemy(string.Empty, new object[]
            {
                Convert.ToSingle(rand.Next(15, 800)),
                Convert.ToSingle(rand.Next(10, 400)),
                Convert.ToSingle(rand.Next(-1, 1)) / 2,
                Convert.ToSingle(rand.Next(-1, 1)) / 2,
                "random",
                "bat"
            });
        }

        public void AddEnemy(string type, object[] @params)
        {
            var enemy = _enemyFactory.Create(@params);

            GameObjects.Add(enemy);
        }

        public void AddBullet(Bullet bullet)
        {
            GameObjects.Add(bullet);
        }

        public void MinigameFinished(bool success)
        {
            if (!success)
            {
                _player.Health--;
            }
        }
    }
}
