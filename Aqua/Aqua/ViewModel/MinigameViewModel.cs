﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Enum;
using Aqua.Game;
using Aqua.Input;
using Aqua.View;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua.ViewModel
{
    public class MinigameViewModel
    {
        private Inputcontroller _inputController;
        private Dictionary<string, bool> _prevState;
        private List<string> _prevAlnums;
        private MinigameTextView _view;
        private readonly IHasGameMode _gameMode;
        private readonly IHasMinigame _game;

        public bool WaitingForUserToExit { get; set; }

        private List<MiniGame> MiniGames { get; } = new List<MiniGame>()
        {
            // Add minigames here
            new MiniGame() {
                Instruction = "Type \"Hello world\"",
                Answer = "Hello world",
                TimeLimit = 300,
                Error = "Heh heh"
            },
            new MiniGame() {
                Instruction = "Type \"The quick brown fox jumped over the lazy dog\"",
                Answer = "The quick brown fox jumped over the lazy dog",
                TimeLimit = 400,
                Error = "Not quick enough"
            },
            new MiniGame() {
                Instruction = "Type \"A\"",
                Answer = "A",
                TimeLimit = 50,
                Error = "Nope, not fast enough"
            },
            new MiniGame() {
                Instruction = "Type \"abcdefghijklmnopqrstuvwxyz\"",
                Answer = "abcdefghijklmnopqrstuvwxyz",
                TimeLimit = 400,
                Error = "You lost"
            },
            new MiniGame() {
                Instruction = "Type \"Yo yo\"",
                Answer = "Yo yo",
                TimeLimit = 100,
                Error = "You're trash :)"
            },
            new MiniGame() {
                Instruction = "Type \"abcdefghijklmmnopqrstuvwxyz\"",
                Answer = "abcdefghijklmmnopqrstuvwxyz",
                TimeLimit = 400,
                Error = "You lose"
            },
            new MiniGame() {
                Instruction = "Type \"Irish wristwatch\"",
                Answer = "Irish wristwatch",
                TimeLimit = 350,
                Error = "Heh, one less life"
            },
            new MiniGame() {
                Instruction = "Type \"A\"",
                Answer = "A",
                TimeLimit = 40,
                Error = "Nope, not fast enough"
            },
            new MiniGame() {
                Instruction = "Type \"ajskdlf\"",
                Answer = "ajskdlf",
                TimeLimit = 150,
                Error = "Bleh, you lost"
            },
            new MiniGame() {
                Instruction = "Type \"mmnmnmvvwvwvmmnmnmnnnmmn\"",
                Answer = "mmnmnmvvwvwvmmnmnmnnnmmn",
                TimeLimit = 300,
                Error = ":("
            },
            new MiniGame() {
                Instruction = "Type \"Through tough thorough thought\"",
                Answer = "Through tough thorough thought",
                TimeLimit = 400,
                Error = ";-;"
            },
            new MiniGame() {
                Instruction = "Type \"nnmnmmwnsmfne\"",
                Answer = "nnmnmmwnsmfne",
                TimeLimit = 300,
                Error = ":("
            },
       };

        public MiniGame CurrentMiniGame { get; private set; }

        public MinigameViewModel(Inputcontroller input, SpriteFont font, IHasGameMode gameMode, IHasMinigame game)
        {
            if (input == null)
            {
                throw new ArgumentException(nameof(input));
            }

            // init prevState
            _prevState = GetState();
            _prevAlnums = new List<string>();

            _inputController = input;
            _view = new MinigameTextView(this, font);
            _gameMode = gameMode;
            _game = game;
        }

        public string UserInput { get; set; } = string.Empty;

        public void HandleUserInput()
        {
            var presses = GetState().Where(x => x.Value != _prevState[x.Key] && x.Value).Select(x => x.Key).ToList();
            presses.AddRange(_inputController.AlNumKeys.Where(x => !_prevAlnums.Contains(x)));

            if (WaitingForUserToExit)
            {
                if (presses.Contains("enter"))
                {
                    WaitingForUserToExit = false;
                    End(_view.Fail);
                    return;
                }
            }

            foreach (var p in presses)
            {
                switch (p)
                {
                    case "enter":

                        _view.Finish();
                        
                        break;
                    case "back":

                        if (UserInput.Length > 0)
                        {
                            UserInput = UserInput.Substring(0, UserInput.Length - 1);
                        }

                        break;

                    case "space":

                        UserInput += " ";

                        break;
                    case "up":
                        break;
                    case "down":
                        break;
                    case "left":
                        break;
                    case "right":
                        break;
                    case "escape":

                        _gameMode.PrevMode = GameMode.MiniGame;
                        _gameMode.GameMode = GameMode.Menu;

                        break;

                    default:

                        UserInput += p;

                        break;
                }
            }

            _prevState = GetState();
            _prevAlnums = _inputController.AlNumKeys.ToList();
        }

        private Dictionary<string, bool> GetState()
        {
            return new Dictionary<string, bool>
            {
                { "enter", _inputController?.enter ?? false },
                { "back", _inputController?.backspace ?? false },
                { "space", _inputController?.space ?? false },
                { "up", _inputController?.up ?? false},
                { "down", _inputController?.down ?? false },
                { "left", _inputController?.left ?? false },
                { "right", _inputController?.right ?? false }
            };
        }

        public void Update()
        {
            HandleUserInput();

            if (CurrentMiniGame == null)
            {
                _view.Clear();
                NewGame();

                _view.Instruction = CurrentMiniGame.Instruction;
                _view.Answer = CurrentMiniGame.Answer;
                _view.TimeLimit = CurrentMiniGame.TimeLimit;
                _view.Time = 0;
                _view.UserInput = UserInput;
                _view.Error = CurrentMiniGame.Error;
            }
            else
            {

                _view.Update();

                if (_view.IsDone)
                {
                    // todo: Notify to switch to game w/ 'fail'
                    _view.Time = 0;
                    End(!_view.Fail);
                }
            }
        }

        private void End(bool success)
        {
            // todo: Pass in the GameViewModel and inc/dec life based on success
            // then switch to GameViewModel
            if (!success && WaitingForUserToExit)
            {
                return;
            }

            
            _gameMode.GameMode = GameMode.MainGame;
            _game.MinigameFinished(success);
            CurrentMiniGame = null;
        }

        private void NewGame()
        {
            _view.Clear();
            CurrentMiniGame = MiniGames[new Random().Next(0, MiniGames.Count - 1)];
        }

        public void Draw(SpriteBatch sprite)
        {
            Update();
            _view?.Draw(sprite);
        }
    }
}
