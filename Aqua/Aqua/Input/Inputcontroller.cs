﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;


namespace Aqua.Input
{
    public class Inputcontroller : INotifyPropertyChanged
    {
        private static Inputcontroller inputContoller;
        private KeyboardState state;
        public Boolean up;
        public Boolean down;
        public Boolean left;
        public Boolean right;
        public bool space;
        public bool backspace;
        public bool enter;
        public bool escape;

        public List<string> AlNumKeys { get; set; }

        private Inputcontroller()
        {
            state = Keyboard.GetState();
            up = false;
            down = false;
            left = false;
            right = false;
            space = false;
            backspace = false;
            enter = false;
            escape = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static Inputcontroller Instance
        {
            get
            {
                if (inputContoller == null)
                    inputContoller = new Inputcontroller();
                return inputContoller;
            }
        }

        public void updateinput()
        {
            state = Keyboard.GetState();
            right = state.IsKeyDown(Keys.Right);
            left = state.IsKeyDown(Keys.Left);
            up = state.IsKeyDown(Keys.Up);
            down = state.IsKeyDown(Keys.Down);
            space = state.IsKeyDown(Keys.Space);
            backspace = state.IsKeyDown(Keys.Back);
            enter = state.IsKeyDown(Keys.Enter);
            escape = state.IsKeyDown(Keys.Escape);

            AlNumKeys = state.GetPressedKeys()
                .Where(x => x >= Keys.A && x <= Keys.Z || x >= Keys.D0 && x <= Keys.D9)
                .Select(x => x.ToString())
                .ToList();

            OnPropertyChanged(up, down, left, right);

        }

        protected void OnPropertyChanged(Boolean up, Boolean down, Boolean left, Boolean right)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs("input"));
            }
        }
    }
}

