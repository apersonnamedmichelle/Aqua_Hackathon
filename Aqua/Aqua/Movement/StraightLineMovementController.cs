﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Aqua.Movement
{
    class StraightLineMovementController : IMovementController
    {
        public void Move(IMoveable moveable)
        {
            var newPosition = new Vector2(
                moveable.Position.X + moveable.Velocity.X, 
                moveable.Position.Y + moveable.Velocity.Y
                );

            moveable.Position = newPosition;
        }
    }
}
