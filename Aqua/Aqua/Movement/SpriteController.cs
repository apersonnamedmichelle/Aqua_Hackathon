﻿using Aqua.Enum;
using Aqua.Model;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Movement
{
    /// <summary>
    /// Sprite controller for the player.
    /// Frames 0-9 are idle, 10-19 are run right, 20-29 are run left.
    /// </summary>
    public class SpriteController
    {
        private MovementState _currentPlayerState;
        private MovementState _previousPlayerState;
        private int _currentFrame;
        private float _currentFrameTime;
        private bool _isMoving;
        private bool _isJumping;
        private static ContentManager _contentManager;
        private static GameTime _gameTime;

        public static ContentManager ContentManager
        {
            protected get { return _contentManager; }
            set { _contentManager = value; }
        }

        public static GameTime GameTime
        {
            protected get { return _gameTime; }
            set { _gameTime = value; }
        }

        public SpriteController(int newCurrentFrame)
        {
            _currentFrame = 1;
            _currentFrameTime = 0;
        }

        public string HandleSpriteAnimation(Player player, GameTime gameTime)
        {
            string retVal = "";
            if (player.Direction == MovementState.Up && _currentFrameTime > 50)
            {
                retVal = "Jump__00" + _currentFrame;
                _currentFrameTime = gameTime.ElapsedGameTime.Milliseconds;
            }
            else if (player.Direction == MovementState.Down)
            {

            }
            else if ((player.Direction == MovementState.Left || player.Direction == MovementState.Right) && _currentFrameTime > 50)
            {
                retVal = "Jump__00" + _currentFrame;
                _currentFrameTime = gameTime.ElapsedGameTime.Milliseconds;
            }
            else if (player.Direction == MovementState.Idle && _currentFrameTime > 310) // Player is idle.
            {
                retVal = "Idle__00" + _currentFrame;
                _currentFrameTime = gameTime.ElapsedGameTime.Milliseconds;
            }

            if (_currentFrame > 8)
            {
                _currentFrame = 0;
            }
            _currentFrame++;
            _currentFrameTime += gameTime.ElapsedGameTime.Milliseconds;


            return retVal;
        }

        public void AnimateRight()
        {

        }

    }
}
