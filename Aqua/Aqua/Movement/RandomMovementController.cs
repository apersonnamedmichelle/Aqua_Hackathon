﻿using System;
using Microsoft.Xna.Framework;

namespace Aqua.Movement
{
    public class RandomMovementController : IMovementController
    {
        private readonly Random _rand = new Random();

        public void Move(IMoveable moveable)
        {
            var velX = moveable.Velocity.X;
            var velY = moveable.Velocity.Y;

            velX += (float) _rand.Next(-1, 2) / 2;
            velY += (float) _rand.Next(-1, 2) / 2;

            if (velX > 5) velX /= 5;
            if (velY > 5) velY /= 5;

            var x = moveable.Position.X + velX;
            var y = moveable.Position.Y + velY;

            if (x > 800)
            {
                x -= Math.Abs(velX);

                if (velX > 0) velX *= -1;
            }
            if (x < 0)
            {
                x += Math.Abs(velX);
                if (velX < 0) velX *= -1;
            }

            if (y < 0)
            {
                y += Math.Abs(velY);
                if (velY < 0) velY *= -1;
            }
            if (y > 420) // blaze it
            {
                y -= Math.Abs(velY);
               if (velY > 0) velY *= -1;
            }

            moveable.Position = new Vector2(x, y);
            moveable.Velocity = new Vector2(velX, velY);
        }
    }
}
