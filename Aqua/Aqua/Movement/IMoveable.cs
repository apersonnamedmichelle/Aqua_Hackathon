﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Aqua.Enum;

namespace Aqua.Movement
{
    public interface IMoveable
    {
        /// <summary>
        /// The position of the object on screen
        /// </summary>
        Vector2 Position { get; set; }

        /// <summary>
        /// The state of the moveable object.
        /// </summary>
        MovementState Direction { get; set; }

        /// <summary>
        /// The velocity of this object.
        /// </summary>
        Vector2 Velocity { get; set; }
    }
}
