﻿using Aqua.Input;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Movement
{
    public class PlayerMovementController : IMovementController
    {
        Inputcontroller userinput;
        int x;
        float y;
        float yvelocity;


        public PlayerMovementController()
        {
            x = 0;
            y = 0;
            yvelocity = 0;
            userinput = Inputcontroller.Instance;
        }
        public void Move(IMoveable moveable)
        {
            x = (int)moveable.Position.X;
            y = (int)moveable.Position.Y;
            if (userinput.up && y == 400)
            {
                jump();
                moveable.Direction = Enum.MovementState.Up;
            }
            else if (userinput.down)
            {
                y += 2;
                moveable.Direction = Enum.MovementState.Down;
            }
            else if (userinput.left)
            {
                x -= 2;
                moveable.Direction = Enum.MovementState.Left;
            }
            else if (userinput.right)
            {
                x += 2;
                moveable.Direction = Enum.MovementState.Right;
            }
            else
            {
                moveable.Direction = Enum.MovementState.Idle;
            }
            if (80 < x && x < 190)
            {
                Console.WriteLine(y);
                if (y != 275)
                {
                    if (y < 425)
                    {
                        if ((y + yvelocity) > 275 && y < 275)
                        {
                            yvelocity = 0;
                            y = 275;
                        }
                        else
                        {
                            yvelocity += .5f;
                        }
                    }
                }
                else
                {
                    yvelocity = 0;
                }

            }
            else if (600 < x && x < 710)
            {
                if (y != 275)
                {
                    if ((y + yvelocity) > 275 && y < 275)
                    {
                        yvelocity = 0;
                        y = 275;
                    }
                    else
                    {
                        yvelocity++;
                    }
                }
                else
                {
                    yvelocity = 0;
                }
            }
            else if (y < 425) { yvelocity += 1; }

            if (x < 1) { x = 2; }
            if (x > 799) { x = 798; }
            y += yvelocity;
            if (y >= 400)
            {
                y = 400;
            }

            moveable.Position = new Microsoft.Xna.Framework.Vector2(x, y);
        }
        public void jump()
        {
            yvelocity = -20;
        }
    }
}
