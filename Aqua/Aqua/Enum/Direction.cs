﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Enum
{
    public enum MovementState
    {
        Up, 
        Down, 
        Left,
        Right,
        Idle
    }
}
