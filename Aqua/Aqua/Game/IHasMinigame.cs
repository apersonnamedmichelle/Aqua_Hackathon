﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Game
{
    public interface IHasMinigame
    {
        void MinigameFinished(bool success);
    }
}
