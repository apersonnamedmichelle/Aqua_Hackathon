﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua
{
    /// <summary>
    /// Class representing a map for tiles (i.e. drawing patterns of tiles to the screen)
    /// </summary>
    public class TileMap
    {
        private List<CollisionTiles> _collisionTiles = new List<CollisionTiles>();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TileMap() { }

        /// <summary>
        /// Gets the list of tiles to collide with the player.
        /// </summary>
        public List<CollisionTiles> CollisionTiles
        {
            get { return _collisionTiles; }
        }

        /// <summary>
        /// Generates a map of the collision tiles.
        /// </summary>
        /// <param name="map"> The 2D array specifying the map</param>
        /// /// <param name="width"> The width of the texture.</param>
        /// /// <param name="height"> The height of the tile.</param>
        public void Generate(int[,] map, int width, int height)
        {
            for (int x = 0; x < map.GetLength(1); x++)
            {
                for (int y = 0; y < map.GetLength(0); y++)
                {
                    int num = map[y, x];
                    if (num > 0)
                    {
                        Console.WriteLine("Width:" + (x * width) + "heigth:" + (y * height));
                        _collisionTiles.Add(new CollisionTiles(num, new Rectangle(x * height, y * height, width, height)));
                    }
                }
            }
        }

        /// <summary>
        /// Draws the collision tiles.
        /// </summary>
        /// <param name="spriteBatch"> The sprite batch used to draw the tiles.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach(var tile in _collisionTiles)
            {
                tile.Draw(spriteBatch);
            }
        }
    }
}
