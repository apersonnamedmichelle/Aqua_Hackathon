﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aqua.Enum;

namespace Aqua.Game
{
    public interface IHasGameMode
    {
        GameMode GameMode { get; set; }
        GameMode PrevMode { get; set; }
    }
}
