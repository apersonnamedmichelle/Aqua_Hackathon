﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua
{
    /// <summary>
    /// Tiles that are collidable.
    /// </summary>
    public class CollisionTiles : Tiles
    {
        public CollisionTiles(int i, Rectangle newRectangle) : base()
        {
            texture = Content.Load<Texture2D>("Tile" + i);
            this.Rectangle = newRectangle;
        }
    }
}
