﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Game
{
    public interface IPlayable
    {
        bool GameOver { get; set; }

        string About { get; }

        void Play();

        void Quit();
    }
}
