﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua
{
    public class Tiles
    {
        protected Texture2D texture;
        private Rectangle _rectangle;
        private static ContentManager _content;

        /// <summary>
        /// Gets or sets the _rectangle member variable.
        /// </summary>
        public Rectangle Rectangle
        {
            get { return _rectangle; }
            protected set { _rectangle = value; }
        }

        /// <summary>
        /// Gets or sets the _content member variable.
        /// </summary>
        public static ContentManager Content
        {
            protected get { return _content; }
            set { _content = value; }
        }

        /// <summary>
        /// Draws the texture for this class on screen.
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, _rectangle, Color.White);
        }
    }
}
