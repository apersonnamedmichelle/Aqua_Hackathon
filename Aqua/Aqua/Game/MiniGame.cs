﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aqua.Game
{
    public class MiniGame
    {
        public string Instruction { get; set; }

        public string Answer { get; set; }

        public int TimeLimit { get; set; }

        public string Error { get; set; }
        
    }
}
