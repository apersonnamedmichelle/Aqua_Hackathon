﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Aqua.Enum;
using Aqua.Factory;
using Aqua.Input;
using Aqua.Model;
using Aqua.Movement;
using Aqua.Update;
using Aqua.ViewModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Aqua
{
    class GameComposer
    {
        public void Compose(Game1 game)
        {
            var texturesByName = new Dictionary<string, Texture2D>();
            var movementByName = new Dictionary<string, IMovementController>();
            var gameObjects = new List<BaseObject>();
            
            game.GameMode = GameMode.Menu;
            game.PrevMode = GameMode.MainGame; 

            var minigameFont = game.Content.Load<SpriteFont>("MiniGameFont");

            texturesByName.Add("bat", game.Content.Load<Texture2D>("kirby"));

            // todo: create input controller
            // todo: create movement controllers, pass to factories
            BaseObject.ContentManager = game.Content;
            
            movementByName.Add("random", new RandomMovementController());
            var bulletFactory = new BulletFactory(movementByName, game.Content);
            var enemyFactory = new EnemyFactory(movementByName, texturesByName, bulletFactory, gameObjects);
            var playerFactory = new PlayerFactory(game.GraphicsDevice, game.Content, bulletFactory, gameObjects);

            var gameViewModel = new GameViewModel(playerFactory, enemyFactory, gameObjects, game, minigameFont);
            var minigameViewModel = new MinigameViewModel(Inputcontroller.Instance, minigameFont, game, gameViewModel);
            var menuViewModel = new MenuViewModel(game, Inputcontroller.Instance, minigameFont, game.GraphicsDevice, game.Content);


            var straightmove = new StraightLineMovementController();
            movementByName.Add("straight", straightmove);

            //var script = XDocument.Load("script").Root?.Element("Events");
            //var scriptReader = new GameScriptReader(gameViewModel, script);

            game.MenuViewModel = menuViewModel;
            game.MainGameViewModel = gameViewModel;
            game.MinigameViewModel = minigameViewModel;
        }
    }
}
